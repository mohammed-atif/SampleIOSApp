//
//  FetchUserStory.swift
//  InstaTab
//
//  Created by Mohammed Atif on 07/01/18.
//  Copyright © 2018 Mohammed Atif. All rights reserved.
//

import Foundation
import Alamofire

class FetchUserStory {
    
    func getUserStory(onComplete : @escaping ([UserStoryData], Int) -> Void, onError : @escaping (Int) -> Void){
        let parameters : Parameters = [
            Constants.JSONData.KEY : Constants.RequestData.KEY
        ]
        Alamofire.request(Constants.RequestData.URL, method: .get, parameters: parameters)
            .validate(statusCode: 200..<300)
            .responseJSON { (response) in
                switch (response.result){
                case .success:
                    var storyDataList : [UserStoryData] = []
                    let responseData = response.data!
                    if let jsonData = try? JSONSerialization.jsonObject(with: responseData, options: []) as? [String: Any]{
                        let storyList = jsonData![Constants.JSONData.HITS] as! [Any]
                        let count  = storyList.count
                        print("count \(count)")
                        for i in 0..<storyList.count {
                            let storyData = storyList[i]
                            storyDataList.append(UserStoryData(storyData: storyData))
                        }
                        onComplete(storyDataList, response.response!.statusCode)
                    }
                case .failure(let error):
                    print(error)
                    onError(response.response!.statusCode)
                }
        }
    }
    
}
