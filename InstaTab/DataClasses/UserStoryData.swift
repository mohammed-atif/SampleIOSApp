//
//  UserStoryData.swift
//  InstaTab
//
//  Created by Mohammed Atif on 07/01/18.
//  Copyright © 2018 Mohammed Atif. All rights reserved.
//

import UIKit

struct UserStoryData {
    var description : String?
    var imageUrl : String?
    var authorId : Int?
    var author : String?
    var authorAvatar : String?
    var liked : Bool?
    var likes : Int?
    var views : Int?
    var isBookMarked : Bool?
    var width : CGFloat?
    var height : CGFloat?
    
    init(storyData : Any) {
        if let storyJson = storyData as? [String : Any] {
            description = storyJson[Constants.JSONData.DESCRIPTION] as? String
            imageUrl = storyJson[Constants.JSONData.IMAGE_URL] as? String
            authorId = storyJson[Constants.JSONData.USER_ID] as? Int
            author = storyJson[Constants.JSONData.AUTHOR] as? String
            authorAvatar = storyJson[Constants.JSONData.USER_AVATAR] as? String
            likes = storyJson[Constants.JSONData.LIKES] as? Int
            views = storyJson[Constants.JSONData.VIEWS] as? Int
            width = storyJson[Constants.JSONData.IMAGE_WIDTH] as? CGFloat
            height = storyJson[Constants.JSONData.IMAGE_HEIGHT] as? CGFloat
            let likeCountObject = storyJson[Constants.JSONData.LIKED] as? Int
            if let likeCounts = likeCountObject {
                liked = likeCounts > 0
            }else{
                liked = false
            }
            isBookMarked = false
        }
    }
    
}
