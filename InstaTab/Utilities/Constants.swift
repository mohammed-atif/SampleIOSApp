//
//  Constants.swift
//  InstaTab
//
//  Created by Mohammed Atif on 06/01/18.
//  Copyright © 2018 Mohammed Atif. All rights reserved.
//

import Foundation

public class Constants {
    public  class Identifiers{
        public static let USER_STORY_HEADER = "header"
        public static let USER_STORY_CELL = "user_story"
        public static let INSTA_STORY_CELL = "insta_story"
    }
    
    public class RequestData {
        public static let URL = "https://pixabay.com/api/"
        public static let KEY = "7617306-aba6f9474747846e539ef99f9"
    }
    
    public class JSONData {
        public static let KEY = "key"
        public static let TOTAL_HITS = "totalHits"
        public static let HITS = "hits"
        public static let DESCRIPTION = "tags"
        public static let IMAGE_URL = "webformatURL"
        public static let USER_ID = "user_id"
        public static let AUTHOR = "user"
        public static let USER_AVATAR = "userImageURL"
        public static let LIKES = "favorites"
        public static let VIEWS = "views"
        public static let LIKED = "likes"
        public static let IMAGE_WIDTH = "imageWidth"
        public static let IMAGE_HEIGHT = "imageHeight"
    }
}
