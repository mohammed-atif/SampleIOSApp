//
//  DownloadedFileModel.swift
//  InstaTab
//
//  Created by Mohammed Atif on 07/01/18.
//  Copyright © 2018 Mohammed Atif. All rights reserved.
//

import RealmSwift

class DownloadFileModel: Object {
    @objc dynamic var url = ""
    @objc dynamic var mappedFile : String?
}
