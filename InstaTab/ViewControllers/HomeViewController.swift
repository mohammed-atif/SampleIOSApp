//
//  FirstViewController.swift
//  InstaTab
//
//  Created by Mohammed Atif on 06/01/18.
//  Copyright © 2018 Mohammed Atif. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    //MARK: Binder Objects
    
    @IBOutlet weak var userStoryTableView: UITableView!
    @IBOutlet weak var progressIndicator: UIActivityIndicatorView!
    
    //MARK: Variables
    
    var userStoryList : [UserStoryData]?
    let fetchUserStory : FetchUserStory = FetchUserStory()
    
    //MARK: Lifecycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        progressIndicator.stopAnimating()
        userStoryTableView.dataSource = self
        userStoryTableView.delegate = self
        userStoryTableView.rowHeight = UITableViewAutomaticDimension
        userStoryTableView.estimatedRowHeight = 100
        userStoryTableView.sectionHeaderHeight = UITableViewAutomaticDimension
        userStoryTableView.estimatedSectionHeaderHeight = 100
        fetchUserStories()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: TableView Methods
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.section == 0){
            let cell : InstaStoryTableCell = tableView.dequeueReusableCell(withIdentifier: Constants.Identifiers.INSTA_STORY_CELL, for: indexPath) as! InstaStoryTableCell
            return cell
        }else{
            let cell : UserStoryTableCell = tableView.dequeueReusableCell(withIdentifier: Constants.Identifiers.USER_STORY_CELL, for: indexPath) as! UserStoryTableCell
            if let storyData = userStoryList?[indexPath.row]{
                cell.userName.text = storyData.author
                cell.location.text = storyData.author
                cell.postLikes.text = "Likes: \(storyData.likes!)"
                cell.totalViews.text = "Views: \(storyData.views!)"
                cell.storyCaption.text = storyData.description
                cell.updateViews(width: storyData.width!, height: storyData.height!, imageUrl: storyData.imageUrl!, avatarUrl: storyData.authorAvatar!)
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell : HeaderTableCell = tableView.dequeueReusableCell(withIdentifier: Constants.Identifiers.USER_STORY_HEADER) as! HeaderTableCell
        if(section == 0){
            cell.storyHeader.text = "Insta Story"
        }else{
            cell.storyHeader.text = "User Story"
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section == 0){
            return 1
        }else{
            if let userStories = userStoryList {
                return userStories.count
            }else{
                return 0
            }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    //MARK: Private Methods
    
    func fetchUserStories() -> Void{
        progressIndicator.startAnimating()
        fetchUserStory.getUserStory(
            onComplete: { (response, code) in
                print("Success response code is \(code)")
                self.userStoryList = response
                self.userStoryTableView.reloadData()
                self.progressIndicator.stopAnimating()
        },
            onError: { (code) in
                print("error resposne code is \(code)")
                self.progressIndicator.stopAnimating()
        })
    }

}

